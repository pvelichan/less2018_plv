<html>
	<head>
		<title>Altera informações</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		
		<style>
		
		#volta{
  font-family: "Arial", cursive, sans-serif;
	background-color: #66cc33;
	font-size: 19px;
	border: 1px solid #555555;
  color:black;
  margin-left: 5.5%;
  cursor: pointer;
}

#volta:hover {
	border-radius: 12px;
	border: 2px solid white;
	
}

		#salvar{
  margin-top: 2%;
  font-family: "Arial", cursive, sans-serif;
	background-color: #66cc33;
	font-size: 22px;
	border: 1px solid #555555;
  color:black;
  margin-left: 2%;
  margin-top: 3%;
  cursor: pointer;
}

#salvar:hover {
	border-radius: 12px;
	border: 2px solid white;
}
body{
   background-color: #BDB76B;
 }
 .form{
	 font-family: "Arial", cursive, sans-serif;
	 font-size: 19px;
	 height:50%;
	 width:30%;
	 margin-left:5%;
	 margin-top:5%;
	 text-align:left;
 }
 .form form{
	 font-family: "Arial", cursive, sans-serif;
	 font-size: 19px;
 }
 #msg{
  font-family: "Arial", cursive, sans-serif;
	background-color: #66cc33;
	font-size: 19px;
	border: 1px solid #555555;
  color:black;
  text-align:center;
  margin-top: 10%;
  margin-left:20%;
}
 #msg2{
  font-family: "Arial", cursive, sans-serif;
	background-color: #66cc33;
	font-size: 19px;
	border: 1px solid #555555;
  color:black;
  text-align: left;
  margin-left: 5%;
  margin-top: 5%;
}
.area{
	height:100%;
	 width:100%;
	 text-align:left;
}
		</style>
	</head>
	<body>
		<?php
			require_once 'FuncoesColeta.php';
			$c= new FuncoesColeta();
			if(isset($_POST['nome'])){
				if(isset($_GET['id'])){
					$c->setid($_GET['id']);
					$c->setnome($_POST['nome']);
					$c->setendereco($_POST['endereco']);
					$c->settelefone($_POST['telefone']);
					$c->setemail($_POST['email']);
					$c->setdia_funciona($_POST['dia_funciona']);
					$c->sethora_funciona($_POST['hora_funciona']);
					$c->alterar();
		?>
		
		<button type="button" id="msg" disabled>Alterado com sucesso</button>
		 <a href="local_coleta.php"><button type="button" id="volta">Voltar</button></a>
		
		<?php
				}
			}else{
				
				if(isset ($_GET['id'])){
					$c->setId($_GET['id']);
					$resp=$c->buscarId();
		?>
		<div class="area">
		
		<button type="button" id="msg2" disabled>Alterar informações</button>
			
		<form class="form" method="POST">
		<p>
			Nome: <input type="text" name="nome"
			value="<?php echo $resp['nome']?>">
		</p>
		<p>
			Endereco: <input type ="text" name="endereco"
			value="<?php echo $resp['endereco']?>">
		</p>
		<p>
			Telefone: <input type ="text" name="telefone"
			value="<?php echo $resp['telefone']?>">
		</p>
		<p>
			Email: <input type ="text" name="email"
			value="<?php echo $resp['email']?>">
		</p>
		
		<p>Dia de Funcionamento: 
			<select name="dia_funciona">
                    <option value=""></option>
                    <option value="Segunda à Sexta">Segunda à Sexta</option>
                    <option value="Segunda à Sábado">Segunda à Sábado</option>
                    <option value="Segunda à Domingo">Segunda à Domingo</option>
                </select>
            </p>
			
			<p>Hora de Funcionamento: 
			<select name="hora_funciona">
                    <option value=""></option>
                    <option value="Das 8Hrs às 17Hrs">Das 8Hrs às 17Hrs</option>
                    <option value="Das 9Hrs às 18Hrs">Das 9Hrs às 18Hrs</option>
                    <option value="Das 10Hrs às 19Hrs">Das 10Hrs às 19Hrs</option>
					<option value="Das 11Hrs às 20Hrs">Das 11Hrs às 20Hrs</option>
					<option value="Das 12Hrs às 21Hrs">Das 12Hrs às 21Hrs</option>
					<option value="Das 13Hrs às 22Hrs">Das 13Hrs às 22Hrs</option>
					<option value="Das 14Hrs às 23Hrs">Das 14Hrs às 23Hrs</option>
					<option value="Das 15Hrs às 00Hrs">Das 15Hrs às 00Hrs</option>
                </select>
            </p>
			
		<p>
			<input id="salvar" type="submit" value="Salvar">
		</p>
		</form>
		<a href='local_coleta.php'><button type="button" id="volta">Voltar</button></a>
		</div>
		<?php
				}
			}
		?>
	</body>
</html>