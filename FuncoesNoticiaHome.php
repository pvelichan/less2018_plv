<?php 
	require_once 'ConexaoBD.php';
	class FuncoesNoticiaHome{
		private $id;
		private $nome;
		private $descricao;
		
		public function getid(){
			return $this->id;
		}
		public function setid($id){
			$this ->id=$id;
		}
		public function getnome(){
			return $this->nome;
		}
		public function setnome($nome){
			$this->nome=$nome;
		}
		public function getdescricao(){
			return $this->descricao;
		}
		public function setdescricao($descricao){
			$this->descricao=$descricao;
		}
		
		public function buscarTodos(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from noticia_home order by nome"
				);
				$stmt -> execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function inserir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"insert into noticia_home(nome, descricao) values(:n,:d)"
				);
				$stmt->bindValue(":n",$this->getnome());
				$stmt->bindValue(":d",$this->getdescricao());
	
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function excluir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"delete from noticia_home where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function buscarId(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from noticia_home where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				$stmt -> execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function alterar(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"update noticia_home set nome=:n,descricao=:d ".
					"where id=:id"
			);
				$stmt->bindValue(":id",$this->getid());
				$stmt->bindValue(":n",$this->getnome());
				$stmt->bindValue(":d",$this->getdescricao());
				
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}
?>