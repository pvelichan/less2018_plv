<?php 
	require_once 'ConexaoBD.php';
	class FuncoesUsuario{
		private $id;
		private $nome;
		private $email;
		private $senha;
		private $telefone;
		
		public function getid(){
			return $this->id;
		}
		public function setid($id){
			$this ->id=$id;
		}
		public function getnome(){
			return $this->nome;
		}
		public function setnome($nome){
			$this->nome=$nome;
		}
		public function getemail(){
			return $this->email;
		}
		public function setemail($email){
			$this->email=$email;
		}
		public function getsenha(){
			return $this->senha;
		}
		public function setsenha($senha){
			$this->senha=$senha;
		}
		public function gettelefone(){
			return $this->telefone;
		}
		public function settelefone($telefone){
			$this->telefone=$telefone;
		}

		public function buscarTodos(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from usuarios order by email"
				);
				$stmt -> execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function inserir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"insert into usuarios(nome, email, senha, telefone) values(:n,:e,:s,:t)"
				);
				$stmt->bindValue(":n",$this->getnome());
				$stmt->bindValue(":e",$this->getemail());
				$stmt->bindValue(":s",$this->getsenha());
				$stmt->bindValue(":t",$this->gettelefone());
	
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function excluir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"delete from usuarios where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function buscarId(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from usuarios where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				$stmt -> execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}
?>