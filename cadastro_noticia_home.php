<html>

<head>

  <link rel="stylesheet" type="text/css" href="cadastro_noticia_home.css">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
    crossorigin="anonymous">
  </script>
</head>

<script language="javascript">

		function check(){

			if(noticia_home.nome!="" && noticia_home.descricao!=""){
	
				formulario.submit();
		
			}else{
	
				alert("preencha os campos!");
		
			}
		}

	</script>


<body>
  <div class="tela_inteira">

    <div class="titulo">
      <div class="logo">
        <a href="pagina_inicial.php">
          <img src='img_lixoconsciente.jpg'/ id="logo" title="Home">
        </a>
      </div>
      <div class="titulo_pag">
        <button type="button" id="titulobutton" disabled>Cadastro de Noticias</button>
      </div>
    </div>



    <div class="parte_baixo">

	  <?php 
			require_once 'FuncoesNoticiaHome.php';
			$c = new FuncoesNoticiaHome();
			if(isset($_POST['nome'])){
				$c->setnome($_POST['nome']);
				$c->setdescricao($_POST['descricao']);
				$c->inserir();
				unset($_POST['nome']);
				header("Refresh:0");
			}
		?>
	  
        <div id="inserir" class="form">
          <form class="formcad" method="POST">
            <p>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nome: <input type="text" name="nome" placeholder="Digite o seu nome" required>
            </p>

            <p>
              Descrição:
            </p>
			<textarea rows="10" cols="95" name="descricao"></textarea>
			
            <button type="submit" value="Inserir" id="salvar">Salvar</button>
            <a href="pagina_inicial.php"><button type="button" id="cancelar">Cancelar</button></a>
          </form>
        </div>

    </div>
  </div>

  <body>

</html>