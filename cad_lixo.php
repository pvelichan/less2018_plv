<html>

<head>

  <link rel="stylesheet" type="text/css" href="cad_lixo.css">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
    crossorigin="anonymous">
  </script>
</head>

<script language="javascript">

		function check(){

			if(cad_lixo.nome!="" && cad_lixo.descricao!="" && cad_lixo.tamanho.value!="" && cad_lixo.peso.value!=""){
	
				formulario.submit();
		
			}else{
	
				alert("preencha os campos!");
		
			}
		}

	</script>


<body>
  <div class="tela_inteira">

    <div class="titulo">
      <div class="logo">
        <a href="pagina_inicial.php">
          <img src='img_lixoconsciente.jpg'/ id="logo" title="Home">
        </a>
      </div>
      <div class="titulo_pag">
        <button type="button" id="titulobutton" disabled>Tela de Cadastro de Lixo/Descartavel</button>
      </div>
    </div>



    <div class="parte_baixo">

      <div class="opcoes">
        <ul>
          <li><a href="pagina_inicial.php" title="Página Inicial">Página Inicial</a></li>
          <li><a href="cad_lixo.php" title="Cadastro de Lixo">Cadastro de Lixo</a></li>
          <li><a href="cad_material.php" title="Cadastro de Material reciclável para entrega">Cadastro de
              Material reciclável para entrega</a></li>
          <li><a href="forum.php" title="Fórum">Fórum</a></li>
          <li><a href="local_coleta.php" title="Locais de Coleta">Locais de Coleta</a></li>
          <li><a href="servico.php" title="Serviços e Horários">Serviços e Horários</a></li>
        </ul>
      </div>

      <div class="cadastro_lixo">

	  <?php 
			require_once 'FuncoesLixo.php';
			$c = new FuncoesLixo();
			if(isset($_POST['nome'])){
				$c->setnome($_POST['nome']);
				$c->setdescricao($_POST['descricao']);
				$c->setrecomendacao($_POST['recomendação']);
				$c->settamanho($_POST['tamanho']);
				$c->setpeso($_POST['peso']);
				$c->inserir();
				unset($_POST['nome']);
				header("Refresh:0");
			}
		?>
	  
        <div id="inserir" class="form">
          <form method="POST">
            <p>
              Nome: <input type="text" name="nome" placeholder="Digite o nome do material/descartavel" required>
            </p>
            <p>
              Descrição: <input type="text" name="descricao" placeholder="Digite a descricao aqui" required>
            </p>
            <p>
              Recomendação p/: <input type="text" name="recomendação" placeholder="Digite recomendação aqui" required>
            </p>
            <p>
              Tamanho: <input type="number" name="tamanho" placeholder="Digite tamanho (em cm) aqui" required>
            </p>
            <p>
              Peso: <input type="number" name="peso" placeholder="Digite o peso (em Kg) aqui" required>
            </p>

            <button type="submit" value="Inserir" id="salvar">Salvar</button>
            <a href="pagina_inicial.php"><button type="button" id="cancelar">Cancelar</button></a>
          </form>
        </div>
      </div>

    </div>
  </div>

  <body>

</html>