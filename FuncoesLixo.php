<?php 
	require_once 'ConexaoBD.php';
	class FuncoesLixo{
		private $id;
		private $nome;
		private $descricao;
		private $recomendacao;
		private $tamanho;
		private $peso;
		
		public function getid(){
			return $this->id;
		}
		public function setid($id){
			$this ->id=$id;
		}
		public function getnome(){
			return $this->nome;
		}
		public function setnome($nome){
			$this->nome=$nome;
		}
		public function getdescricao(){
			return $this->descricao;
		}
		public function setdescricao($descricao){
			$this->descricao=$descricao;
		}
		public function getrecomendacao(){
			return $this->recomendacao;
		}
		public function setrecomendacao($recomendacao){
			$this->recomendacao=$recomendacao;
		}
		public function gettamanho(){
			return $this->tamanho;
		}
		public function settamanho($tamanho){
			$this->tamanho=$tamanho;
		}
		public function getpeso(){
			return $this->peso;
		}
		public function setpeso($peso){
			$this->peso=$peso;
		}

		public function buscarTodos(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from cad_lixo order by nome"
				);
				$stmt -> execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function inserir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"insert into cad_lixo(nome, descricao, recomendacao, tamanho, peso) values(:n,:d,:r,:t,:p)"
				);
				$stmt->bindValue(":n",$this->getnome());
				$stmt->bindValue(":d",$this->getdescricao());
				$stmt->bindValue(":r",$this->getrecomendacao());
				$stmt->bindValue(":t",$this->gettamanho());
				$stmt->bindValue(":p",$this->getpeso());
	
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function excluir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"delete from cad_lixo where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function buscarId(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from cad_lixo where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				$stmt -> execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function alterar(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"update cad_lixo set nome=:n,descricao=:d,recomendacao=:r,tamanho=:t,peso=:p ".
					"where id=:id"
			);
				$stmt->bindValue(":id",$this->getid());
				$stmt->bindValue(":n",$this->getnome());
				$stmt->bindValue(":d",$this->getdescricao());
				$stmt->bindValue(":r",$this->getrecomendacao());
				$stmt->bindValue(":t",$this->gettamanho());
				$stmt->bindValue(":p",$this->getpeso());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}
?>