<?php 
	require_once 'ConexaoBD.php';
	class FuncoesMaterial{
		private $id;
		private $nome;
		private $descricao;
		private $recomendacao;
		private $tamanho;
		private $peso;
		private $tipo;
		
		public function getid(){
			return $this->id;
		}
		public function setid($id){
			$this ->id=$id;
		}
		public function getnome(){
			return $this->nome;
		}
		public function setnome($idnome){
			$this->nome=$idnome;
		}
		public function getdescricao(){
			return $this->descricao;
		}
		public function setdescricao($descricao){
			$this->descricao=$descricao;
		}
		public function getrecomendacao(){
			return $this->recomendacao;
		}
		public function setrecomendacao($recomendacao){
			$this->recomendacao=$recomendacao;
		}
		public function gettamanho(){
			return $this->tamanho;
		}
		public function settamanho($tamanho){
			$this->tamanho=$tamanho;
		}
		public function getpeso(){
			return $this->peso;
		}
		public function setpeso($peso){
			$this->peso=$peso;
		}
		public function gettipo(){
			return $this->tipo;
		}
		public function settipo($tipo){
			$this->tipo=$tipo;
		}

		public function buscarTodos(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from cad_material order by nome"
				);
				$stmt -> execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function inserir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"insert into cad_material(nome, descricao, recomendacao, tamanho, peso, tipo) values(:n,:d,:r,:t,:p,:ti)"
				);
				$stmt->bindValue(":n",$this->getnome());
				$stmt->bindValue(":d",$this->getdescricao());
				$stmt->bindValue(":r",$this->getrecomendacao());
				$stmt->bindValue(":t",$this->gettamanho());
				$stmt->bindValue(":p",$this->getpeso());
				$stmt->bindValue(":ti",$this->gettipo());
	
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function excluir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"delete from cad_material where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function buscarId(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from cad_material where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				$stmt -> execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function alterar(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"update cad_material set nome=:n,descricao=:d,recomendacao=:r,tamanho=:t,peso=:p,tipo=:ti ".
					"where id=:id"
			);
				$stmt->bindValue(":id",$this->getid());
				$stmt->bindValue(":n",$this->getnome());
				$stmt->bindValue(":d",$this->getdescricao());
				$stmt->bindValue(":r",$this->getrecomendacao());
				$stmt->bindValue(":t",$this->gettamanho());
				$stmt->bindValue(":p",$this->getpeso());
				$stmt->bindValue(":ti",$this->gettipo());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}
?>