<html>

<head>

  <link rel="stylesheet" type="text/css" href="cad_lixo.css">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
    crossorigin="anonymous">
  </script>
</head>

	<script language="javascript">

		function check(){

			if(cad_usuario.nome!="" && cad_usuario.senha.value!="" && cad_usuario.email!=""){
	
				formulario.submit();
		
			}else{
	
				alert("preencha os campos!");
		
			}
		}

	</script>

<body>
  <div class="tela_inteira">

    <div class="titulo">
      <div class="logo">
        <a href="pagina_inicial.php">
          <img src='img_lixoconsciente.jpg'/ id="logo" title="Home">
        </a>
      </div>
      <div class="titulo_pag">
        <button type="button" id="titulobutton" disabled>Tela de Cadastro de Usuario</button>
      </div>
    </div>

	<?php 
			require_once 'FuncoesUsuario.php';
			$c = new FuncoesUsuario();
			if(isset($_POST['nome'])){
				$c->setnome($_POST['nome']);
				$c->setemail($_POST['email']);
				$c->setsenha($_POST['senha']);
				$c->settelefone($_POST['telefone']);
				$c->inserir();
				unset($_POST['nome']);
				header("Refresh:0");
			}
		?>

    <div id="inserir" class="parte_baixo">


      <div class="cadastro_lixo">

        <div class="form">
          <form method="POST">
            <p>
              Nome: <input type="text" name="nome" placeholder="Digite seu nome aqui" required>
            </p>
            <p>
              Email: <input type="text" name="email" placeholder="Digite seu email aqui" required>
            </p>
            <p>
              Senha: <input type="password" name="senha" placeholder="Digite sua senha aqui" required>
            </p>
            <p>
              Telefone <input type="text" name="telefone" placeholder="Digite seu número de contato">
            </p>


            <a href="index.php"><button type="submit" value="Inserir" id="salvar">Salvar</button></a>
            <a href="index.php"><button type="button" id="cancelar">Cancelar</button></a>
          </form>
        </div>
      </div>

    </div>
  </div>

  <body>

</html>