<?php 
	require_once 'ConexaoBD.php';
	class FuncoesColeta{
		private $id;
		private $nome;
		private $endereco;
		private $telefone;
		private $email;
		private $dia_funciona;
		private $hora_funciona;
		
		public function getid(){
			return $this->id;
		}
		public function setid($id){
			$this ->id=$id;
		}
		public function getnome(){
			return $this->nome;
		}
		public function setnome($nome){
			$this->nome=$nome;
		}
		public function getendereco(){
			return $this->endereco;
		}
		public function setendereco($endereco){
			$this->endereco=$endereco;
		}
		public function gettelefone(){
			return $this->telefone;
		}
		public function settelefone($telefone){
			$this->telefone=$telefone;
		}
		public function getemail(){
			return $this->email;
		}
		public function setemail($email){
			$this->email=$email;
		}
		public function getdia_funciona(){
			return $this->dia_funciona;
		}
		public function setdia_funciona($dia_funciona){
			$this->dia_funciona=$dia_funciona;
		}
		public function gethora_funciona(){
			return $this->hora_funciona;
		}
		public function sethora_funciona($hora_funciona){
			$this->hora_funciona=$hora_funciona;
		}
		
		public function buscarTodos(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from local_coleta order by nome"
				);
				$stmt -> execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function inserir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"insert into local_coleta(nome, endereco, telefone, email, dia_funciona, hora_funciona) values(:n,:en,:t,:e,:df,:hf)"
				);
				$stmt->bindValue(":n",$this->getnome());
				$stmt->bindValue(":en",$this->getendereco());
				$stmt->bindValue(":t",$this->gettelefone());
				$stmt->bindValue(":e",$this->getemail());
				$stmt->bindValue(":df",$this->getdia_funciona());
				$stmt->bindValue(":hf",$this->gethora_funciona());
				
				
	
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function excluir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"delete from local_coleta where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function buscarId(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from local_coleta where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				$stmt -> execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function alterar(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"update local_coleta set nome=:n,endereco=:en,telefone=:te,email=:e,dia_funciona=:df,hora_funciona=:hf ".
					"where id=:id"
			);
				$stmt->bindValue(":id",$this->getid());
				$stmt->bindValue(":n",$this->getnome());
				$stmt->bindValue(":en",$this->getendereco());
				$stmt->bindValue(":te",$this->gettelefone());
				$stmt->bindValue(":e",$this->getemail());
				$stmt->bindValue(":df",$this->getdia_funciona());
				$stmt->bindValue(":hf",$this->gethora_funciona());			
				
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}
?>