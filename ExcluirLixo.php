<html>
	<head>
		<title>Excluir Lixo</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	</head>
	<style>
	#volta{
  font-family: "Arial", cursive, sans-serif;
	background-color: #66cc33;
	font-size: 19px;
	border: 1px solid #555555;
  color:black;
  margin-left: 5.5%;
  cursor: pointer;
}

#volta:hover {
	border-radius: 12px;
	border: 2px solid white;
	
}

#msg{
  font-family: "Arial", cursive, sans-serif;
	background-color: #66cc33;
	font-size: 19px;
	border: 1px solid #555555;
  color:black;
 text-align:center;
  margin-left: 5%;
  margin-top: 5%;
}
.mensagem{
	height:50%;
	width:50%;
	margin-left:25%;
}
.area{
	height:100%;
	 width:100%;
	 text-align:left;
}
body{
   background-color: #BDB76B;
 }
 
	</style>
	
	<body>
		<div class="area">
		<?php
			require_once 'FuncoesLixo.php';
			if(isset($_GET['id'])){
				$c= new FuncoesLixo();
				$c->setid($_GET['id']);
				$c->excluir();
				?>
				<button type="button" id="msg" disabled>Excluído com Sucesso</button>
				<a href="pagina_inicial.php"><button type="button" id="volta">Voltar</button></a>
		<?php
			}else{
				echo "<p> Erro! Informação ausente</p>";
		?>
				<a href="pagina_inicial.php"><button type="button" id="volta">Voltar</button></a>
		<?php
			}
		?>
		</div>
	</body>
</html>