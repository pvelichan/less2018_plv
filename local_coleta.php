<html>

<head>

  <link rel="stylesheet" type="text/css" href="servico.css">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
    crossorigin="anonymous">
  </script>
</head>

<body>
  <div class="tela_inteira">

    <div class="titulo">

      <div class="logo">
        <a href="pagina_inicial.php">
          <img src='img_lixoconsciente.jpg'/ id="logo" title="Home">
        </a>
      </div>
		
	<div class="titulo_pag">
        <button type="button" id="titulobutton" disabled>Locais de coleta de Lixo</button>
      </div>
		
      <div class="pesquisa">
        <p>
          Pesquisa: <input type="search" name="pesquisa" placeholder="Pesquise o material aqui">
          <button type="button" id="adicionar" title="Adicionar Noticia"><a href="cadastro_noticia_local_coleta.php">Adicionar local de coleta</a></button>
          <button type="button" id="sair" title="Sair"><a href="index.php">Sair</a></button>
        </p>

      </div>

    </div>

    <div class="parte_baixo">

      <div class="opcoes">
        <ul>
          <li><a href="pagina_inicial.php" title="Página Inicial">Página Inicial</a></li>
          <li><a href="cad_lixo.php" title="Cadastro de Lixo">Cadastro de Lixo</a></li>
          <li><a href="cad_material.php" title="Cadastro de Material reciclável para entrega">Cadastro de
              Material reciclável para entrega</a></li>
          <li><a href="forum.php" title="Fórum">Fórum</a></li>
          <li><a href="local_coleta.php" title="Locais de Coleta">Locais de Coleta</a></li>
          <li><a href="servico.php" title="Serviços e Horários">Serviços e Horários</a></li>
        </ul>
      </div>

      <div class="conteudo">

	  
	  <?php
			require_once 'FuncoesColeta.php';
			$c = new FuncoesColeta();
			$dados=$c->buscarTodos();
				echo "<table class='customers'>";
				echo "<tr><th>Nome</th><th>Endereço</th><th>Telefone</th><th>Email</th><th>Dia de Funcionamento</th><th>Hora de Funcionamento</th></tr>";
				foreach($dados as $linha){
					print "<tr>";
					print "<td>".$linha[1]."</td>";
					print "<td>".$linha[2]."</td>";
					print "<td>".$linha[3]."</td>";
					print "<td>".$linha[4]."</td>";
					print "<td>".$linha[5]."</td>";
					print "<td>".$linha[6]."</td>";
					
					print "<td><a href='AlterarLocal.php?id=".$linha['id']."'>Alterar</a></td>";
					print "<td><a href='ExcluirLocal.php?id=".$linha['id']."'>Excluir</a></td>";
					print "</tr>";
				}
				echo "</table>";
		?>
	  
      </div>

    </div>

  </div>

  <body>

</html>