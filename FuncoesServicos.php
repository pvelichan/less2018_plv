<?php 
	require_once 'ConexaoBD.php';
	class Funcoesservicos{
		private $id;
		private $nome;
		private $tipo;
		private $descricao;
		private $dia_funciona;
		private $hora_funciona;
		private $telefone;
		private $email;
		
		
		public function getid(){
			return $this->id;
		}
		public function setid($id){
			$this ->id=$id;
		}
		public function getnome(){
			return $this->nome;
		}
		public function setnome($nome){
			$this->nome=$nome;
		}
		public function gettipo(){
			return $this->tipo;
		}
		public function settipo($tipo){
			$this->tipo=$tipo;
		}
		public function getdescricao(){
			return $this->descricao;
		}
		public function setdescricao($descricao){
			$this->descricao=$descricao;
		}
		public function getdia_funciona(){
			return $this->dia_funciona;
		}
		public function setdia_funciona($dia_funciona){
			$this->dia_funciona=$dia_funciona;
		}
		public function gethora_funciona(){
			return $this->hora_funciona;
		}
		public function sethora_funciona($hora_funciona){
			$this->hora_funciona=$hora_funciona;
		}
		public function gettelefone(){
			return $this->telefone;
		}
		public function settelefone($telefone){
			$this->telefone=$telefone;
		}
		public function getemail(){
			return $this->email;
		}
		public function setemail($email){
			$this->email=$email;
		}
		

		public function buscarTodos(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from servicos order by nome"
				);
				$stmt -> execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function inserir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"insert into servicos(nome, tipo, descricao, dia_funciona, hora_funciona, telefone, email) values(:n,:ti,:d,:df,:hf,:t,:e)"
				);
				$stmt->bindValue(":n",$this->getnome());
				$stmt->bindValue(":ti",$this->gettipo());
				$stmt->bindValue(":d",$this->getdescricao());
				$stmt->bindValue(":df",$this->getdia_funciona());
				$stmt->bindValue(":hf",$this->gethora_funciona());
				$stmt->bindValue(":t",$this->gettelefone());
				$stmt->bindValue(":e",$this->getemail());
				
	
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function excluir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"delete from servicos where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function buscarId(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from servicos where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				$stmt -> execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function alterar(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"update servicos set nome=:n,tipo=:t,descricao=:d,dia_funciona=:df,hora_funciona=:hf,telefone=:te,email=:e ".
					"where id=:id"
			);
				$stmt->bindValue(":id",$this->getid());
				$stmt->bindValue(":n",$this->getnome());
				$stmt->bindValue(":t",$this->gettipo());
				$stmt->bindValue(":d",$this->getdescricao());
				$stmt->bindValue(":df",$this->getdia_funciona());
				$stmt->bindValue(":hf",$this->gethora_funciona());
				$stmt->bindValue(":te",$this->gettelefone());
				$stmt->bindValue(":e",$this->getemail());
				
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}
?>