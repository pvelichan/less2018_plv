<html>
	<head>
		<title>Altera informações</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		
		<style>
		
		#volta{
  font-family: "Arial", cursive, sans-serif;
	background-color: #66cc33;
	font-size: 19px;
	border: 1px solid #555555;
  color:black;
  margin-left: 5.5%;
  cursor: pointer;
}

#volta:hover {
	border-radius: 12px;
	border: 2px solid white;
	
}

		#salvar{
  margin-top: 2%;
  font-family: "Arial", cursive, sans-serif;
	background-color: #66cc33;
	font-size: 22px;
	border: 1px solid #555555;
  color:black;
  margin-left: 2%;
  margin-top: 3%;
  cursor: pointer;
}

#salvar:hover {
	border-radius: 12px;
	border: 2px solid white;
}
body{
   background-color: #BDB76B;
 }
 .form{
	 font-family: "Arial", cursive, sans-serif;
	 font-size: 19px;
	 height:50%;
	 width:30%;
	 margin-left:5%;
	 margin-top:5%;
	 text-align:left;
 }
 .form form{
	 font-family: "Arial", cursive, sans-serif;
	 font-size: 19px;
 }
 #msg{
  font-family: "Arial", cursive, sans-serif;
	background-color: #66cc33;
	font-size: 19px;
	border: 1px solid #555555;
  color:black;
  text-align:center;
  margin-top: 10%;
  margin-left:20%;
}
 #msg2{
  font-family: "Arial", cursive, sans-serif;
	background-color: #66cc33;
	font-size: 19px;
	border: 1px solid #555555;
  color:black;
  text-align: left;
  margin-left: 5%;
  margin-top: 5%;
}
.area{
	height:100%;
	 width:100%;
	 text-align:left;
}
		</style>
	</head>
	<body>
		<?php
			require_once 'FuncoesLixo.php';
			$c= new FuncoesLixo();
			if(isset($_POST['nome'])){
				if(isset($_GET['id'])){
					$c->setid($_GET['id']);
					$c->setnome($_POST['nome']);
					$c->setdescricao($_POST['descricao']);
					$c->setrecomendacao($_POST['recomendacao']);
					$c->settamanho($_POST['tamanho']);
					$c->setpeso($_POST['peso']);
					$c->alterar();
		?>
		
		<button type="button" id="msg" disabled>Alterado com sucesso</button>
		 <a href="pagina_inicial.php"><button type="button" id="volta">Voltar</button></a>
		
		<?php
				}
			}else{
				
				if(isset ($_GET['id'])){
					$c->setId($_GET['id']);
					$resp=$c->buscarId();
		?>
		<!--<div class="imagem">
			<img src="estudo.jpg " height="100%" width="100%">
		</div>-->
		<div class="area">
		
		<button type="button" id="msg2" disabled>Alterar informações</button>
			
		<form class="form" method="POST">
		<p>
			Nome: <input type="text" name="nome"
			value="<?php echo $resp['nome']?>">
		</p>
		<p>
			Descrição: <input type ="text" name="descricao"
			value="<?php echo $resp['descricao']?>">
		</p>
		<p>
			Recomendação: <input type ="text" name="recomendacao"
			value="<?php echo $resp['recomendacao']?>">
		</p>
		<p>
			Tamanho: <input type ="number" name="tamanho"
			value="<?php echo $resp['tamanho']?>">
		</p>
		<p>
			Peso: <input type ="number" name="peso"
			value="<?php echo $resp['peso']?>">
		</p>
		<p>
			<input id="salvar" type="submit" value="Salvar">
		</p>
		</form>
		<a href='pagina_inicial.php'><button type="button" id="volta">Voltar</button></a>
		</div>
		<?php
				}
			}
		?>
	</body>
</html>