-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 02-Dez-2018 às 03:02
-- Versão do servidor: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gestao_coletalixo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_lixo`
--

CREATE TABLE `cad_lixo` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `recomendacao` varchar(200) NOT NULL,
  `tamanho` float NOT NULL,
  `peso` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cad_lixo`
--

INSERT INTO `cad_lixo` (`id`, `nome`, `descricao`, `recomendacao`, `tamanho`, `peso`) VALUES
(4, 'Garrafa pet', 'Garrafa 2litros de plÃ¡stico', 'Reciclagem ', 45, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad_material`
--

CREATE TABLE `cad_material` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `recomendacao` varchar(200) NOT NULL,
  `tamanho` float NOT NULL,
  `peso` float NOT NULL,
  `tipo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cad_material`
--

INSERT INTO `cad_material` (`id`, `nome`, `descricao`, `recomendacao`, `tamanho`, `peso`, `tipo`) VALUES
(3, 'Lata de alumÃ­nio', 'Lata de refrigerante ', 'reciclÃ¡vel em qualquer local', 15, 1, 'metal');

-- --------------------------------------------------------

--
-- Estrutura da tabela `local_coleta`
--

CREATE TABLE `local_coleta` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `endereco` varchar(300) NOT NULL,
  `telefone` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `dia_funciona` varchar(200) NOT NULL,
  `hora_funciona` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `local_coleta`
--

INSERT INTO `local_coleta` (`id`, `nome`, `endereco`, `telefone`, `email`, `dia_funciona`, `hora_funciona`) VALUES
(3, 'ColetaConsciente', 'Rua Napoli, 226', '45240171', 'peterson.velichan@gmail.com', 'Segunda Ã  Sexta', 'Das 8Hrs Ã s 17Hrs');

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticia_home`
--

CREATE TABLE `noticia_home` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `descricao` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `noticia_home`
--

INSERT INTO `noticia_home` (`id`, `nome`, `descricao`) VALUES
(7, 'Novo usuÃ¡rio', 'Oi, sou um novo usuÃ¡rio do Atitude Consciente.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE `servicos` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `tipo` varchar(200) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `dia_funciona` varchar(200) NOT NULL,
  `hora_funciona` varchar(200) NOT NULL,
  `telefone` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `nome`, `tipo`, `descricao`, `dia_funciona`, `hora_funciona`, `telefone`, `email`) VALUES
(14, 'Lixeiro', 'Coleta de Lixo', 'Servico onde o caminhÃ£o passa para recolher o lixo das casas', 'Segunda Ã  SÃ¡bado', 'Das 9Hrs Ã s 18Hrs', '975535117', 'peter@gmail.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `email` varchar(200) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `telefone` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `email`, `senha`, `telefone`) VALUES
(7, 'peterson', 'peterson.velichan@gmail.com', '1234', '975535117');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cad_lixo`
--
ALTER TABLE `cad_lixo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cad_material`
--
ALTER TABLE `cad_material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `local_coleta`
--
ALTER TABLE `local_coleta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noticia_home`
--
ALTER TABLE `noticia_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicos`
--
ALTER TABLE `servicos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cad_lixo`
--
ALTER TABLE `cad_lixo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cad_material`
--
ALTER TABLE `cad_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `local_coleta`
--
ALTER TABLE `local_coleta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `noticia_home`
--
ALTER TABLE `noticia_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `servicos`
--
ALTER TABLE `servicos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
