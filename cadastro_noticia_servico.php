<html>

<head>

  <link rel="stylesheet" type="text/css" href="cadastro_noticia_servico.css">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
    crossorigin="anonymous">
  </script>
</head>

<script language="javascript">

		function check(){

			if(servicos.nome!="" && servicos.descricao!="" && servicos.tipo!="" && servicos.dia_funciona.value!="" && servicos.hora_funciona.value!="" && servicos.telefone!="" && servicos.email=""){
	
				formulario.submit();
		
			}else{
	
				alert("preencha os campos!");
		
			}
		}

	</script>


<body>
  <div class="tela_inteira">

    <div class="titulo">
      <div class="logo">
        <a href="pagina_inicial.php">
          <img src='img_lixoconsciente.jpg'/ id="logo" title="Home">
        </a>
      </div>
      <div class="titulo_pag">
        <button type="button" id="titulobutton" disabled>Cadastro de Serviços</button>
      </div>
    </div>



    <div class="parte_baixo">

	  <?php 
			require_once 'FuncoesServicos.php';
			$c = new FuncoesServicos();
			if(isset($_POST['nome'])){
				$c->setnome($_POST['nome']);
				$c->settipo($_POST['tipo']);
				$c->setdescricao($_POST['descricao']);
				$c->setdia_funciona($_POST['dia_funciona']);
				$c->sethora_funciona($_POST['hora_funciona']);
				$c->settelefone($_POST['telefone']);
				$c->setemail($_POST['email']);
				$c->inserir();
				unset($_POST['nome']);
				header("Refresh:0");
			}
		?>
	  
        <div id="inserir" class="form">
          <form class="formcad" method="POST">
            <p>
              Nome: <input type="text" name="nome" placeholder="Digite o seu nome" required>
            </p>

			<p>
              Tipo de serviço: <input type="text" name="tipo" placeholder="Digite o seu nome" required>
            </p>
			
            <p>
              Descrição:
            </p>
			<textarea rows="10" cols="95" name="descricao"></textarea>
			
			<p>Dia de Funcionamento: 
			<select name="dia_funciona">
                    <option value=""></option>
                    <option value="Segunda à Sexta">Segunda à Sexta</option>
                    <option value="Segunda à Sábado">Segunda à Sábado</option>
                    <option value="Segunda à Domingo">Segunda à Domingo</option>
                </select>
            </p>
			
			<p>Hora de Funcionamento: 
			<select name="hora_funciona">
                    <option value=""></option>
                    <option value="Das 8Hrs às 17Hrs">Das 8Hrs às 17Hrs</option>
                    <option value="Das 9Hrs às 18Hrs">Das 9Hrs às 18Hrs</option>
                    <option value="Das 10Hrs às 19Hrs">Das 10Hrs às 19Hrs</option>
					<option value="Das 11Hrs às 20Hrs">Das 11Hrs às 20Hrs</option>
					<option value="Das 12Hrs às 21Hrs">Das 12Hrs às 21Hrs</option>
					<option value="Das 13Hrs às 22Hrs">Das 13Hrs às 22Hrs</option>
					<option value="Das 14Hrs às 23Hrs">Das 14Hrs às 23Hrs</option>
					<option value="Das 15Hrs às 00Hrs">Das 15Hrs às 00Hrs</option>
                </select>
            </p>
			
			<p>
              Telefone: <input type="text" name="telefone" placeholder="Digite o Telefone" required>
            </p>
			
			<p>
             Email: <input type="text" name="email" placeholder="Digite o Email" required>
            </p>
			
            <button type="submit" value="Inserir" id="salvar">Salvar</button>
            <a href="pagina_inicial.php"><button type="button" id="cancelar">Cancelar</button></a>
          </form>
        </div>

    </div>
  </div>

  <body>

</html>